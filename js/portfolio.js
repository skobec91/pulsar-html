/**
  portfolio.js - v1.0

  Functions relating to the functionality of the Portfolio section.
*/

/* ==========================================================================
   Functions
   ========================================================================== */

// Update the left margin of the horizontal portfolio listing if the browser window width is < 1600px
// so the company logos are aligned with the 960px container
function inovia_fix_portfolio_listing_margin() {
  var $window_width = jQuery(window).width(),
      $margin;

  if ($window_width > 1060) {
    $margin = 0;
  } else {
    $margin = 50;
  }

  jQuery("div#filter-categories").css("margin-left", $margin + "px");
  jQuery("div#portfolio-listing-scrollable-container").css("margin-left", $margin + "px");
}

// Update the width of the horizontal portfolio section whenever we filter the company list
function inovia_update_portfolio_listing_width() {
  var $num_portfolio_companies_shown = jQuery('div#portfolio-listing a:not(.isotope-hidden)').size(),
      $logo_width = 200, // Each portfolio company logo is 200px wide
      $padding = 65, // There is 65px of padding between each logo
      $container_width;

  // Calculate the absolute portfolio listing container width; always display logos across 3 rows (if possible)
  if ($num_portfolio_companies_shown <= 12) {
    $container_width = ($logo_width * 3) + ($padding * 4) + 'px';
  } else {
    $container_width = ($logo_width * Math.ceil($num_portfolio_companies_shown / 3)) + ($padding * Math.ceil($num_portfolio_companies_shown / 3)) + 'px';
  }

  // Update the container width dynamically
  jQuery('div#portfolio-listing').css('width', $container_width);
}

// Resize the portfolio container so jq.isotope displays properly in the horizontally-scrolling section
function inovia_resize_portfolio_listing_container(delay) {
  delay = typeof delay !== 'undefined' ? delay : 400; // Default to a delay of 400ms to give the filtering animation time to complete

  setTimeout(function() {
    jQuery('div#portfolio-listing-scrollable-container').resize();
  }, delay);
}

// Quick function to completely refresh the portfolio section
function inovia_refresh_portfolio_section(el_cur_portfolio_company) {
  inovia_update_portfolio_section(el_cur_portfolio_company);
  inovia_update_tweet_author_section('portfolio', el_cur_portfolio_company);
  inovia_load_tweet('portfolio');
}

function inovia_update_portfolio_section(active_portfolio_company) {
  // Cache elements
  var $el_portfolio_headliner = jQuery('section#portfolio h1'),
      $el_company_information_div = jQuery('div#company-information'),
      $el_ceo_icon = jQuery('img.ceo-icon'),
      $el_ceo_name = $el_company_information_div.find('p.name'),
      $el_ceo_position = $el_company_information_div.find('p.position'),
      $el_company_location = $el_company_information_div.find('p.location'),
      $el_company_url = $el_company_information_div.find('a.url'),
      $el_angellist_link = $el_company_information_div.find('a.angellist_link'),
      $el_company_description = $el_company_information_div.find('p.description'),
      $ceo_icon_src = active_portfolio_company.data('contact-headshot'),
      $ceo_name = active_portfolio_company.data('contact-name'),
      $ceo_position = active_portfolio_company.data('contact-position'),
      $company_name = active_portfolio_company.data('company-name'),
      $company_location = active_portfolio_company.data('company-location'),
      $company_url = active_portfolio_company.data('company-url'),
      $angellist_link = active_portfolio_company.data('angellist-link'),
      $company_description = active_portfolio_company.data('company-description');

  // Strip 'http://', 'www.', and trailing slashes from urls
  var $company_base_url = $company_url.replace(/(http|https):\/\//, '').replace('www.', '').replace(/\/+$/, '');

  // Ensure portfolio headline is hidden and company information section is displayed
  if ($el_company_information_div.hasClass('hidden')) {
    $el_portfolio_headliner.hide();
    $el_company_information_div.removeClass('hidden');
  }

  // Make current company logo active
  $el_company_logo_currently_active.removeClass('active');
  $el_company_logo_currently_active.find('img').attr('src', $el_company_logo_currently_active.data('logo-grayscale'));
  active_portfolio_company.addClass('active');
  active_portfolio_company.find('img').attr('src', active_portfolio_company.data('logo-color'));
  $el_company_logo_currently_active = active_portfolio_company;

  // Update portfolio section with correct information
  $el_ceo_icon.attr('src', $ceo_icon_src).attr('alt', $ceo_name);
  $el_ceo_name.html($ceo_name);
  // The following works because String.replace() only replaces the first occurrence of '%s' each time (js has no sprintf() method)
  $el_ceo_position.html(inovia_settings.tweet_person_position_with_span_text.replace('%s', $ceo_position).replace('%s', $company_name));
  $el_company_url.attr('href', $company_url).html($company_base_url);
  $el_company_description.html($company_description);

  if ($company_location.length > 0) {
    $el_company_location.html($company_location).show();
  } else {
    $el_company_location.hide();
  }
  if ($angellist_link.length > 0) {
    $el_angellist_link.attr('href', $angellist_link).show();
  } else {
    $el_angellist_link.hide();
  }
}

function inovia_portfolio_title_show_next_scroll_list_item() {
  ++scroll_list_index;

  $scroll_list_els.eq(scroll_list_index % $scroll_list_els.length)
    .fadeIn(1000)
    .delay(2000)
    .fadeOut(1000, inovia_portfolio_title_show_next_scroll_list_item);
}

(function($) {

  /* ==========================================================================
     Event handlers
     ========================================================================== */

  // Fix the portfolio listing margin on browser resize
  $(window).resize(function() {
    inovia_fix_portfolio_listing_margin();
  });

  // Update portfolio section whenever user clicks on a logo
  $('div#portfolio-listing a').click(function(e) {
    var $el_tweet_aside = $('aside#tweet-portfolio');

    e.preventDefault();
    $.bbq.pushState('#/portfolio/' + this.hash.slice(1));
    inovia_refresh_portfolio_section($(this));
  });

  $('div#filter-categories ul li a').click(function() {
    $portfolio_logos_container.isotope({
      filter: $(this).attr('data-filter'),
      animationOptions: {
        duration: 750,
        easing: 'linear',
        queue: false
      }
    });

    // Toggle the active state of the filter links
    $('div#filter-categories ul li a.active').removeClass('active');
    $(this).addClass('active');

    // Update the portfolio container width to hold the new set of logos
    inovia_update_portfolio_listing_width();
    inovia_resize_portfolio_listing_container();

    return false;
  });

})(this.jQuery); // End of jQuery closure (protecting '$')
