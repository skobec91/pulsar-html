/**
  hash_links.js - v1.0

  Functions for handling hash links and smooth scrolling.
*/

(function($) {

  /* ==========================================================================
     Html5 history (allows linking to portfolio companies, team members, and blog)
     ========================================================================== */

  // Page snapping for internal links
  $('a[href*="#"]:not(.portfolio-logo, .team-icon, .nav-tabs a, .comments-link, #blog-article a)').live('click', function() {
    if (this.hash) {
      $.bbq.pushState('#/' + this.hash.slice(1));
      $(window).trigger('hashchange');
      return false;
    }
  });

  // Capture hashchange events to update page appropriately
  $(window).bind('hashchange', function(e) {
    var $target = location.hash.replace(/^#\/?/, ''),
        $matches;

    if (document.getElementById($target)) {
      $.smoothScroll({
        scrollTarget: '#' + $target,
        speed: 2000
      });
    }

    // Load a portfolio company if requested
    if ($matches = $target.match(/portfolio\/(.+)/)) {
      inovia_refresh_portfolio_section($('#' + $matches[1]));

      // Only scroll to the portfolio section if it's out of view (so we auto-scroll to the portfolio section on page load)
      if ($(document).scrollTop() != $('#portfolio').offset().top) {
        $.smoothScroll({
          scrollTarget: '#portfolio',
          speed: 800
        });
      }
    }
    // Load a team member if requested
    else if ($matches = $target.match(/team\/(.+)/)) {
      inovia_refresh_team_section($('#' + $matches[1]));

      // Only scroll to the team section if it's out of view (so we auto-scroll to the team section on page load)
      if ($(document).scrollTop() != $('#team').offset().top) {
        $.smoothScroll({
          scrollTarget: '#team',
          speed: 800
        });
      }
    }
    // Show blog sidebar if requested
    else if ($target.indexOf('blog') >= 0) {
      setTimeout(function() { inovia_show_blog_sidebar(); }, 1000);

      // Showing extra pages of posts requires a different prettylink than a single post
      if ($matches = $target.match(/blog\/page\/(.+)/)) {
        inovia_update_blog_view(window.location.origin + window.location.pathname + '/blog/page/' + $matches[1]);
      } else if ($matches = $target.match(/blog\/(.+)/)) {
        inovia_update_blog_view(window.location.origin + window.location.pathname + '/' + $matches[1]);
      }
    }
    // Show news sidebar if requested
    else if ($target.indexOf('news') >= 0) {
      setTimeout(function() { inovia_show_news_sidebar(); }, 1000);
    }
  });

  // Check for links in hash fragment on page load
  $(window).trigger('hashchange');

})(this.jQuery); // End of jQuery closure (protecting '$')
